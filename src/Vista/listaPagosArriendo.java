package Vista;

import Controlador.ControladorArriendoEquipos;
import Excepciones.ArriendoException;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import java.awt.event.*;

public class listaPagosArriendo extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JTable tablaDatos;
    private JLabel codigoArriendo;
    private long codigo;

    public listaPagosArriendo(long codigo) {
        this.codigo = codigo;
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);


        buttonCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        });

        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        // call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(e -> onCancel(), KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
        String [] titulo={"Monto", "Fecha", "Tipo pago"};
        String [][] pagos= new String[0][];
        try {
            pagos = ControladorArriendoEquipos.getInstance().listaPagosDeUnArriendo(codigo);
        } catch (ArriendoException e) {
            throw new RuntimeException(e);
        }
        TableModel modelo = new DefaultTableModel(pagos, titulo);
        tablaDatos.setModel(modelo);
        codigoArriendo.setText(String.valueOf(codigo));
    }

    private void onOK() {
        // add your code here
        dispose();
    }

    private void onCancel() {
        // add your code here if necessary
        dispose();
    }

    public static void main(String[] args) {
        listaPagosArriendo dialog = new listaPagosArriendo(0);
        dialog.pack();
        dialog.setVisible(true);
        System.exit(0);
    }
}
