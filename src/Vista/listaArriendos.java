package Vista;

import Controlador.ControladorArriendoEquipos;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import java.awt.event.*;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

public class listaArriendos extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JTextField fechaInicioTextField;
    private JTextField fechaTerminoTextField;
    private JTable tablaArriendos;

    public listaArriendos() {
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);
        fechaInicioTextField.setText("12/12/2022");
        fechaTerminoTextField.setText("12/12/2023");

        buttonOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onOK();
            }
        });

        buttonCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        });

        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        // call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
        String[] titulos = {"CODIGO", "ARRENDADO EL", "DEVUELTO EL", "ESTADO", "RUT CLIENTE", "NOMBRE CLIENTE", "MONTO TOTAL"};
        String[][] datosArriendos = {{}};

        TableModel modeloDeTabla = new DefaultTableModel(datosArriendos, titulos);
        tablaArriendos.setModel(modeloDeTabla);
    }

    private void onOK() {
        String fechaInicioStr = fechaInicioTextField.getText();
        String fechaTerminoStr = fechaTerminoTextField.getText();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        LocalDate fechaInicio, fechaTermino;
        try {
            fechaInicio = LocalDate.parse(fechaInicioStr, formatter);
            fechaTermino = LocalDate.parse(fechaTerminoStr, formatter);
        } catch (DateTimeParseException e) {
            JOptionPane.showMessageDialog(this, "Error fecha con formato erroneo", "Error", JOptionPane.ERROR_MESSAGE );
            return;
        }
        String[] titulos = {"CODIGO", "ARRENDADO EL", "DEVUELTO EL", "ESTADO", "RUT CLIENTE", "NOMBRE CLIENTE", "MONTO TOTAL"};
        String[][] datosArriendo = ControladorArriendoEquipos.getInstance().listaArriendo(fechaInicio, fechaTermino);

        TableModel modelo = new DefaultTableModel(datosArriendo, titulos);
        tablaArriendos.setModel(modelo);

    }

    private void onCancel() {
        // add your code here if necessary
        dispose();
    }

    public static void main(String[] args) {
        listaArriendos dialog = new listaArriendos();
        dialog.pack();
        dialog.setVisible(true);
        System.exit(0);
    }
}
