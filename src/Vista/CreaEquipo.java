package Vista;

import Controlador.ControladorArriendoEquipos;
import Excepciones.EquipoException;

import javax.swing.*;
import java.awt.event.*;

public class CreaEquipo extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JTextField codigoImplemento;
    private JTextField descripcionImplemento;
    private JTextField precioArriendo;

    public CreaEquipo() {
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);

        buttonOK.addActionListener(e -> onOK());

        buttonCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        });

        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        // call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
    }

    private void onOK() {
        String codigoString = codigoImplemento.getText();
        String descripcion = descripcionImplemento.getText();
        String precioString = precioArriendo.getText();
        if(descripcion.trim().equals("")){
            JOptionPane.showMessageDialog(this, "Descripción no puede estar vacia", "Error", JOptionPane.ERROR_MESSAGE);
            return;
        }

        try {
            long codigo = Long.parseLong(codigoString);
            long precio = Long.parseLong(precioString);
            if(codigo>=0 && precio >=0) {
                ControladorArriendoEquipos.getInstance().creaImplemento(codigo, descripcion, precio);
            }else{
                JOptionPane.showMessageDialog(this, "No puede ingresar valores negativos", "Error", JOptionPane.ERROR_MESSAGE);
                return;
            }
        } catch (EquipoException e) {
            JOptionPane.showMessageDialog(this, "Ya existe un equipo con el codigo dado", "Error", JOptionPane.WARNING_MESSAGE );
            return;
        } catch (NumberFormatException e) {
            JOptionPane.showMessageDialog(this, "Error, no corresponde un número valido", "Error", JOptionPane.ERROR_MESSAGE );
            return;
        }
        JOptionPane.showMessageDialog(this, "Se creo un implemento correctamente", "Success", JOptionPane.INFORMATION_MESSAGE);
    }

    private void onCancel() {
        // add your code here if necessary
        dispose();
    }

    public static void main(String[] args) {
        CreaEquipo dialog = new CreaEquipo();
        dialog.pack();
        dialog.setVisible(true);
        System.exit(0);
    }
}
