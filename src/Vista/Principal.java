package Vista;

import Controlador.ControladorArriendoEquipos;
import Excepciones.ArriendoException;

import javax.swing.*;
import java.awt.event.*;

public class Principal extends JDialog {

    private JPanel contentPane;
    private JButton abrirButton;
    private JButton guardarButton;
    private JButton arriendaEquipoButton;
    private JButton devuelveEquipoButton;
    private JButton listadoClientesButton;
    private JButton nuevoClienteButton;
    private JButton nuevoImplementoButton;
    private JButton nuevoConjuntoButton;
    private JButton pagoArriendoButton;
    private JButton listadoArriendosButton;
    private JButton listadoArriendButton;
    private JButton listadoEquiposButton;
    private JButton listadoPagosArriendoButton;
    private JButton detalleDeUnAButton;
    private JButton salirButton;

    public Principal() {
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(abrirButton);

        abrirButton.addActionListener(e -> {
            try {
                ControladorArriendoEquipos.getInstance().readDatosSistema();
                JOptionPane.showMessageDialog(this, "Se cargan los datos de forma exitosa", "Todo chill", JOptionPane.INFORMATION_MESSAGE);
            } catch (ArriendoException ex) {
                JOptionPane.showMessageDialog(this, ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
            }
        });

        guardarButton.addActionListener(e -> guardar());

        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        // call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(e -> onCancel(), KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);

        listadoArriendButton.addActionListener(e -> {
            listaArriendos ventana = new listaArriendos();
            ventana.pack();
            ventana.setVisible(true);
        });

        salirButton.addActionListener(e -> dispose());
        pagoArriendoButton.addActionListener(e -> {
            PagarArriendo ventana=new PagarArriendo();
            ventana.pack();
            ventana.setVisible(true);

        });
        listadoPagosArriendoButton.addActionListener(e -> {
            String codigoString=JOptionPane.showInputDialog(this, "Ingrese el codigo del arriendo");
            String[][]  arriendos = ControladorArriendoEquipos.getInstance().listaArriendosPagados();
            if (arriendos.length == 0) {
                JOptionPane.showMessageDialog(this, "No existen arriendos pagados", "Error", JOptionPane.ERROR_MESSAGE);
                return;
            }
            long codigo;
            try{
                codigo=Long.parseLong(codigoString);
            }catch (NumberFormatException a){
                JOptionPane.showMessageDialog(this, "No se ha ingresados datos", "Error", JOptionPane.ERROR_MESSAGE);
                return;
            }
            if(ControladorArriendoEquipos.getInstance().buscaArriendo(codigo)!=null){
                listaPagosArriendo ventana=new listaPagosArriendo(codigo);
                ventana.pack();
                ventana.setVisible(true);
            }
        });
        nuevoImplementoButton.addActionListener(e -> {
            CreaEquipo ventana=new CreaEquipo();
            ventana.pack();
            ventana.setVisible(true);
        });
    }

    private void guardar() {
        try {
            ControladorArriendoEquipos.getInstance().saveDatosSistema();
            JOptionPane.showMessageDialog(this, "Se guardaron los datos de forma exitosa", "Todo chill", JOptionPane.INFORMATION_MESSAGE);
        } catch (ArriendoException ex) {
            JOptionPane.showMessageDialog(this, ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
        }
    }

    private void onCancel() {
        // add your code here if necessary
        dispose();
    }

    public static void main(String[] args) {
        Principal dialog = new Principal();
        dialog.pack();
        dialog.setVisible(true);
        System.exit(0);
    }
}
