package Vista;

import Controlador.ControladorArriendoEquipos;
import Excepciones.ArriendoException;

import javax.swing.*;
import java.awt.event.*;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class PagarArriendo extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JRadioButton creditoRadioButton;
    private JRadioButton debitoRadioButton;
    private JRadioButton contadoRadioButton;
    private JTextField montoTextField;
    private JTextField numeroTransaccion;
    private JTextField numeroTarjeta;
    private JTextField numeroCuotas;
    private JTextField codigoArriendo;
    private JButton buscarButton;
    private JLabel nombreTextField;
    private JLabel rutTextField;
    private JLabel estadoTextField;
    private JLabel montoTotalTextField;
    private JLabel montoPagadoTextField;
    private JLabel saldoAdeudadoTextField;
    private JLabel fechaTextField;

    public PagarArriendo() {
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);
        LocalDate fecha = LocalDate.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        String fechaFormateada = fecha.format(formatter);
        fechaTextField.setText(fechaFormateada);


        buttonOK.addActionListener(e -> {
            if (contadoRadioButton.isSelected()) {
                String montoString=montoTextField.getText();
                long monto;
                try{
                    monto=Long.parseLong(montoString);
                    if(monto<=0){
                        JOptionPane.showMessageDialog(this, "No pueden ser valores negativos", "Error", JOptionPane.ERROR_MESSAGE);
                        return;
                    }
                } catch (NumberFormatException efs){
                    JOptionPane.showMessageDialog(this, "Error, monto no puede tener caracter numerico", "Error", JOptionPane.ERROR_MESSAGE);
                    return;
                }
                String codigoString=codigoArriendo.getText();
                long codigo;
                try{
                    codigo=Long.parseLong(codigoString);
                } catch(NumberFormatException ee) {
                    JOptionPane.showMessageDialog(this, "codigo debe ser numero" , "Error", JOptionPane.ERROR_MESSAGE);
                    return;
                }
                try {
                    ControladorArriendoEquipos.getInstance().pagaArriendoContado(codigo,monto);
                } catch (ArriendoException ex) {
                    JOptionPane.showMessageDialog(this, ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
                }
            }

            if(debitoRadioButton.isSelected()){
                String montoString=montoTextField.getText();
                long monto;
                try{
                    monto=Long.parseLong(montoString);
                    if(monto<=0){
                        JOptionPane.showMessageDialog(this, "No pueden ser valores negativos", "Error", JOptionPane.ERROR_MESSAGE);
                        return;
                    }
                } catch (NumberFormatException efs){
                    JOptionPane.showMessageDialog(this, "Error, monto no puede tener caracter numerico", "Error", JOptionPane.ERROR_MESSAGE);
                    return;
                }
                String codigoString=codigoArriendo.getText();
                long codigo;
                try{
                    codigo=Long.parseLong(codigoString);
                } catch(NumberFormatException ee) {
                    JOptionPane.showMessageDialog(this, "codigo debe ser numero" , "Error", JOptionPane.ERROR_MESSAGE);
                    return;
                }
                String transaccionString=numeroTransaccion.getText();
                if(transaccionString.trim().equals("")){
                    JOptionPane.showMessageDialog(this, "Numero transaccion no puede estar vacia", "Error", JOptionPane.ERROR_MESSAGE);
                    return;
                }
                String tarjetaString=numeroTarjeta.getText();
                if(transaccionString.trim().equals("")){
                    JOptionPane.showMessageDialog(this, "Numero transaccion no puede estar vacia", "Error", JOptionPane.ERROR_MESSAGE);
                    return;
                }
                try {
                    ControladorArriendoEquipos.getInstance().pagaArriendoDebito(codigo,monto,transaccionString,tarjetaString);
                } catch (ArriendoException ex) {
                    JOptionPane.showMessageDialog(this, ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
                }
            }
            if(creditoRadioButton.isSelected()){
                String montoString=montoTextField.getText();
                long monto;
                try{
                    monto=Long.parseLong(montoString);
                    if(monto<=0){
                        JOptionPane.showMessageDialog(this, "No pueden ser valores negativos", "Error", JOptionPane.ERROR_MESSAGE);
                        return;
                    }
                } catch (NumberFormatException efs){
                    JOptionPane.showMessageDialog(this, "Error, monto no puede tener caracter numerico", "Error", JOptionPane.ERROR_MESSAGE);
                    return;
                }
                String codigoString=codigoArriendo.getText();
                long codigo;
                try{
                    codigo=Long.parseLong(codigoString);
                } catch(NumberFormatException ee) {
                    JOptionPane.showMessageDialog(this, "codigo debe ser numero" , "Error", JOptionPane.ERROR_MESSAGE);
                    return;
                }
                String transaccionString=numeroTransaccion.getText();
                if(transaccionString.trim().equals("")){
                    JOptionPane.showMessageDialog(this, "Numero transaccion no puede estar vacia", "Error", JOptionPane.ERROR_MESSAGE);
                    return;
                }
                String tarjetaString=numeroTarjeta.getText();
                if(tarjetaString.trim().equals("")){
                    JOptionPane.showMessageDialog(this, "Numero tarjeta no puede estar vacia", "Error", JOptionPane.ERROR_MESSAGE);
                    return;
                }
                String cuotasString=numeroCuotas.getText();
                if(cuotasString.trim().equals("")){
                    JOptionPane.showMessageDialog(this, "Numero de cuotas no puede estar vacio", "Error", JOptionPane.ERROR_MESSAGE);
                    return;
                }
                int cuotas = 0;
                try{
                    cuotas=Integer.parseInt(cuotasString);
                }catch (NumberFormatException epa){
                    JOptionPane.showMessageDialog(this, "Numero de cuotas debe ser numerico", "Error", JOptionPane.ERROR_MESSAGE);
                    return;
                }
                try {
                    ControladorArriendoEquipos.getInstance().pagaArriendoCredito(codigo,monto,transaccionString,tarjetaString,cuotas);
                } catch (ArriendoException ex) {
                    JOptionPane.showMessageDialog(this, ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
                }
            }
        });

        buttonCancel.addActionListener(e -> onCancel());

        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        // call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(e -> onCancel(), KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);

        contadoRadioButton.addActionListener(e -> {
            montoTextField.setEnabled(true);
            numeroTransaccion.setEnabled(false);
            numeroTarjeta.setEnabled(false);
            numeroCuotas.setEnabled(false);

        });

        debitoRadioButton.addActionListener(e -> {
            montoTextField.setEnabled(true);
            numeroTransaccion.setEnabled(true);
            numeroTarjeta.setEnabled(true);
            numeroCuotas.setEnabled(false);

        });

        creditoRadioButton.addActionListener(e -> {
            montoTextField.setEnabled(true);
            numeroTransaccion.setEnabled(true);
            numeroTarjeta.setEnabled(true);
            numeroCuotas.setEnabled(true);
        });
        buscarButton.addActionListener(e -> buscar());
    }

    private void buscar(){
        String codigoString=codigoArriendo.getText();
        long codigo;
        try{
            codigo=Long.parseLong(codigoString);
        } catch(NumberFormatException e) {
            JOptionPane.showMessageDialog(this, "Codigo debe ser numero" , "Warning", JOptionPane.WARNING_MESSAGE);
            return;
        }
        String[] datos = ControladorArriendoEquipos.getInstance().consultaArriendoAPagar(codigo);

        if (datos.length == 0) {
            JOptionPane.showMessageDialog(this, "El arriendo no existe", "Warning", JOptionPane.WARNING_MESSAGE);
            return;
        }

        estadoTextField.setText(datos[1]);
        rutTextField.setText(datos[2]);
        nombreTextField.setText(datos[3]);
        montoTotalTextField.setText(datos[4]);
        montoPagadoTextField.setText(datos[5]);
        saldoAdeudadoTextField.setText(datos[6]);

    }

    private void onOK() {
        // add your code here
        dispose();
    }

    private void onCancel() {
        // add your code here if necessary
        dispose();
    }

    public static void main(String[] args) {
        PagarArriendo dialog = new PagarArriendo();
        dialog.pack();
        dialog.setVisible(true);
        System.exit(0);
    }
}
